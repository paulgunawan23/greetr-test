<?php

namespace Simplexi\Greetr;

class Greetr2
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}
